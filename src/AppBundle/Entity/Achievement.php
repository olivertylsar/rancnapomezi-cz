<?php
/**
 * Created by PhpStorm.
 * User: Jirka
 * Date: 12/29/2017
 * Time: 11:43 AM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="achievement")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AchievementRepository")
 */
class Achievement
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="category", type="string", length=50)
     */
    private $category;

    /**
     * @var string
     *
     * @ORM\Column(name="award_driver", type="string", length=50, nullable=true)
     */
    private $awardDriver;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Horse", inversedBy="achievements")
     * @ORM\JoinColumn(name="horse_id", referencedColumnName="id")
     */
    private $horse;

    /**
     * @var string
     *
     * @ORM\Column(name="race", type="string", length=50)
     */
    private $race;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\AchievementCategory", inversedBy="achievements")
     * @ORM\JoinColumn(name="achievement_category_id", referencedColumnName="id")
     */
    private $achievementCategory;


    public function __construct()
    {
        $this->date = new \DateTime();
        // your own logic
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date): void
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param string $category
     */
    public function setCategory(string $category): void
    {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getAwardDriver()
    {
        return $this->awardDriver;
    }

    /**
     * @param string $awardDriver
     */
    public function setAwardDriver($awardDriver): void
    {
        $this->awardDriver = $awardDriver;
    }

    /**
     * @return mixed
     */
    public function getHorse()
    {
        return $this->horse;
    }

    /**
     * @param mixed $horse
     */
    public function setHorse($horse): void
    {
        $this->horse = $horse;
    }

    /**
     * @return mixed
     */
    public function getRace()
    {
        return $this->race;
    }

    /**
     * @param string $race
     */
    public function setRace(string $race): void
    {
        $this->race = $race;
    }

    /**
     * @return mixed
     */
    public function getAchievementCategory()
    {
        return $this->achievementCategory;
    }

    /**
     * @param mixed $achievementCategory
     */
    public function setAchievementCategory($achievementCategory): void
    {
        $this->achievementCategory = $achievementCategory;
    }
}