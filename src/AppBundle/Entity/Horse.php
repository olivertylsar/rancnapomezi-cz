<?php
/**
 * Created by PhpStorm.
 * User: Jirka
 * Date: 12/29/2017
 * Time: 11:43 AM
 */

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity
 * @ORM\Table(name="horse")
 */
class Horse
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50)
     */
    private $name;

    /**
     * @ORM\Column(type="date")
     */
    private $birthDate;

    /**
     * @var string
     *
     * @ORM\Column(name="father", type="string", length=100)
     */
    private $father;

    /**
     * @var string
     *
     * @ORM\Column(name="mother", type="string", length=100)
     */
    private $mother;

    /**
     * @var string
     *
     * @ORM\Column(name="birthPlace", type="string", length=50, nullable=true)
     */
    private $birthPlace;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=50, nullable=true)
     */
    private $color;

    /**
     * @var string
     *
     * @ORM\Column(name="children", type="text", nullable=true)
     */
    private $children;

    /**
     *@Assert\File(
     *  maxSize = "5M",
     *  mimeTypes = {"image/jpeg", "image/png"},
     *  mimeTypesMessage = "Nepovolený formát, povelné formáty jsou: jpg, png",
     *  maxSizeMessage="Maximální povolená velikost je 5MB",
     * )
     * @ORM\Column(name="family_tree", type="string", length=50, nullable=true)
     */
    private $familyTree;

    /**
     * @var string
     *
     * @ORM\Column(name="family_tree_file_name", type="text", nullable=true)
     */
    private $familyTreeFileName;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Achievement", mappedBy="horse")
     * @ORM\OrderBy({"achievementCategory" = "ASC"})
     */
    private $achievements;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\HorsePhoto", mappedBy="horse", orphanRemoval=true, cascade={"persist", "remove"})
     */
    private $horsePhotos;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\HorseCategory", inversedBy="horses")
     * @ORM\JoinColumn(name="horse_category_id", referencedColumnName="id")
     */
    private $horseCategory;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\SexCategory", inversedBy="horses")
     * @ORM\JoinColumn(name="sex_category_id", referencedColumnName="id")
     */
    private $sexCategory;

    /**
     * @Gedmo\Slug(fields={"name", "id"})
     * @ORM\Column(length=128, unique=true)
     */
    private $slug;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="sports", type="boolean")
	 */
	private $sports;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="horse_sale", type="boolean")
	 */
	private $horseSale;

	/**
	 *@Assert\File(
	 *  maxSize = "5M",
	 *  mimeTypes = {"image/jpeg", "image/png"},
	 *  mimeTypesMessage = "Nepovolený formát, povelné formáty jsou: jpg, png",
	 *  maxSizeMessage="Maximální povolená velikost je 5MB",
	 * )
	 * @ORM\Column(name="main_photo", type="string", length=50, nullable=true)
	 */
	private $mainPhoto;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="main_photo_file_name", type="text", nullable=true)
	 */
	private $mainPhotoFileName;

	/**
	 * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Harnessing", inversedBy="horses")
	 * @ORM\JoinTable(name="harnessings")
	 */
	private $harnessings;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="comment", type="text", nullable=true)
	 */
	private $comment;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="harnessing_tax", type="string", length=100, nullable=true)
	 */
	private $harnessingTax;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="owner", type="string", length=125, nullable=true)
	 */
	private $owner;

	/**
	 * Horse constructor.
	 */
    public function __construct()
    {
        $this->horsePhotos = new ArrayCollection();
        $this->achievements = new ArrayCollection();
        $this->harnessings = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * @param mixed $birthDate
     */
    public function setBirthDate($birthDate): void
    {
        $this->birthDate = $birthDate;
    }

    /**
     * @return mixed
     */
    public function getFather()
    {
        return $this->father;
    }

    /**
     * @param string $father
     */
    public function setFather(string $father): void
    {
        $this->father = $father;
    }

    /**
     * @return mixed
     */
    public function getMother()
    {
        return $this->mother;
    }

    /**
     * @param string $mother
     */
    public function setMother(string $mother): void
    {
        $this->mother = $mother;
    }

    /**
     * @return mixed
     */
    public function getBirthPlace()
    {
        return $this->birthPlace;
    }

    /**
     * @param string $birthPlace
     */
    public function setBirthPlace($birthPlace): void
    {
        $this->birthPlace = $birthPlace;
    }

    /**
     * @return mixed
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param string $color
     */
    public function setColor($color): void
    {
        $this->color = $color;
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param string $children
     */
    public function setChildren($children): void
    {
        $this->children = $children;
    }

    /**
     * @return UploadedFile
     */
    public function getFamilyTree()
    {
        return $this->familyTree;
    }

    /**
     * @param $familyTree
     */
    public function setFamilyTree($familyTree = null): void
    {
        $this->familyTree = $familyTree;
    }

    /**
     * @return mixed
     */
    public function getFamilyTreeFileName()
    {
        return $this->familyTreeFileName;
    }

    /**
     * @param string $familyTreeFileName
     */
    public function setFamilyTreeFileName(string $familyTreeFileName): void
    {
        $this->familyTreeFileName = $familyTreeFileName;
    }

    /**
     * @return mixed
     */
    public function getAchievements()
    {
        return $this->achievements;
    }

    /**
     * @param mixed $achievements
     */
    public function setAchievements($achievements): void
    {
        $this->achievements = $achievements;
    }

    /**
     * @return mixed
     */
    public function getHorsePhotos()
    {
        return $this->horsePhotos;
    }

    /**
     * @param mixed $horsePhotos
     */
    public function setHorsePhotos($horsePhotos): void
    {
        $this->horsePhotos = $horsePhotos;
    }

    public function addHorsePhoto(HorsePhoto $horsePhoto)
    {
        $this->horsePhotos[] = $horsePhoto;
        $horsePhoto->setHorse($this);
        return $this;
    }

    public function removeHorsePhoto(HorsePhoto $horsePhoto)
    {
        $this->horsePhotos->removeElement($horsePhoto);
    }

    /**
     * @return mixed
     */
    public function getHorseCategory()
    {
        return $this->horseCategory;
    }

    /**
     * @param mixed $horseCategory
     */
    public function setHorseCategory($horseCategory): void
    {
        $this->horseCategory = $horseCategory;
    }

    /**
     * @return mixed
     */
    public function getSexCategory()
    {
        return $this->sexCategory;
    }

    /**
     * @param mixed $sexCategory
     */
    public function setSexCategory($sexCategory): void
    {
        $this->sexCategory = $sexCategory;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

	/**
	 * @return bool
	 */
	public function isSports() {
		return $this->sports;
	}

	/**
	 * @param bool $sports
	 */
	public function setSports(bool $sports): void {
		$this->sports = $sports;
	}

	/**
	 * @return bool
	 */
	public function isHorseSale() {
		return $this->horseSale;
	}

	/**
	 * @param bool $horseSale
	 */
	public function setHorseSale(bool $horseSale): void {
		$this->horseSale = $horseSale;
	}

	/**
	 * @return UploadedFile
	 */
	public function getMainPhoto()
	{
		return $this->mainPhoto;
	}

	/**
	 * @param $mainPhoto
	 */
	public function setMainPhoto($mainPhoto = null): void
	{
		$this->mainPhoto = $mainPhoto;
	}

	/**
	 * @return string
	 */
	public function getMainPhotoFileName() {
		return $this->mainPhotoFileName;
	}

	/**
	 * @param string $mainPhotoFileName
	 */
	public function setMainPhotoFileName(string $mainPhotoFileName): void {
		$this->mainPhotoFileName = $mainPhotoFileName;
	}

	/**
	 * @return mixed
	 */
	public function getHarnessings() {
		return $this->harnessings;
	}

	/**
	 * @param mixed $harnessings
	 */
	public function setHarnessings($harnessings): void {
		$this->harnessings = $harnessings;
	}

	/**
	 * @return string
	 */
	public function getComment() {
		return $this->comment;
	}

	/**
	 * @param string $comment
	 */
	public function setComment($comment): void {
		$this->comment = $comment;
	}

	/**
	 * @return string
	 */
	public function getHarnessingTax() {
		return $this->harnessingTax;
	}

	/**
	 * @param string $harnessingTax
	 */
	public function setHarnessingTax($harnessingTax): void {
		$this->harnessingTax = $harnessingTax;
	}

	/**
	 * @return string
	 */
	public function getOwner() {
		return $this->owner;
	}

	/**
	 * @param string $owner
	 */
	public function setOwner(string $owner): void {
		$this->owner = $owner;
	}

	/**
	 * @return array
	 */
	public function getAchievementsByYears()
    {
        $achievementsByYear = array();
        foreach ($this->achievements as $achievement) {
            /** @var Achievement $achievement */
            $achievementsByYear[$achievement->getDate()->format('Y')][] = $achievement;
        }
        krsort($achievementsByYear);

        return $achievementsByYear;
    }

}