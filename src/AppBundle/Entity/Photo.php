<?php
/**
 * Created by PhpStorm.
 * User: Jirka
 * Date: 12/29/2017
 * Time: 12:03 PM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="photo")
 */
class Photo
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *@Assert\File(
     *  maxSize = "1M",
     *  mimeTypes = {"image/jpeg", "image/png"},
     *  mimeTypesMessage = "Nepovolený formát, povelné formáty jsou: jpg, png",
     *  maxSizeMessage="Maximální povolená velikost je 1MB",
     * )
     * @ORM\Column(name="file", type="string", length=50)
     */
    private $file;

    /**
     * @var string
     *
     * @ORM\Column(name="file_name", type="string", length=50)
     */
    private $fileName;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Gallery", inversedBy="photos")
     * @ORM\JoinColumn(name="gallery", referencedColumnName="id")
     */
    private $gallery;

    public function __construct()
    {
        // your own logic
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }
    /**
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @return string
     */
    public function getFileName(): string
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     */
    public function setFileName(string $fileName): void
    {
        $this->fileName = $fileName;
    }

    /**
     * @param $file
     */
    public function setFile($file = null): void
    {
        $this->file = $file;
    }

    /**
     * @return mixed
     */
    public function getGallery()
    {
        return $this->gallery;
    }

    /**
     * @param mixed $gallery
     */
    public function setGallery($gallery): void
    {
        $this->gallery = $gallery;
    }
}