<?php
/**
 * Created by PhpStorm.
 * User: Shopsys
 * Date: 11.06.2018
 * Time: 21:44
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity
 * @ORM\Table(name="harnessing")
 */
class Harnessing {
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=50)
	 */
	private $name;

	/**
	 * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Horse", mappedBy="harnessings")
	 */
	private $horses;

	/**
	 * Harnessing constructor.
	 */
	public function __construct() {
		$this->horses = new ArrayCollection();
	}

	/**
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId($id): void {
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name): void {
		$this->name = $name;
	}

	/**
	 * @return mixed
	 */
	public function getHorses() {
		return $this->horses;
	}

	/**
	 * @param mixed $horses
	 */
	public function setHorses($horses): void {
		$this->horses = $horses;
	}
}