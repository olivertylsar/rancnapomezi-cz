<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Achievement;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UspechyController extends Controller
{
    /**
     * @Route("/uspechy", name="uspechy")
     */
    public function indexAction(Request $request)
    {
        $achievementRepository = $this->getDoctrine()->getRepository(Achievement::class);
        $years = $achievementRepository->getAchievementYears();
        $yearsOnAchievement = array();
        foreach ($years as $year){
            $successes = $achievementRepository->findByYear($year['year']);
            $helpArray = array();
            foreach ($successes as $success){
                $categoryName = $success->getAchievementCategory()->getName();
                if ($categoryName != 'Speciální' && $categoryName != '1. místo' && $categoryName != '2. místo' && $categoryName != '3. místo'){
                    $helpArray['ostatní'][] = $success;
                }
                else{
                    $helpArray[$categoryName][] = $success;
                }
            }
            $yearsOnAchievement[$year['year']] = $helpArray;
            $renderYears[] = $year['year'];
        }
        $yearsOnAchievement = $this->sortArrayByArray($yearsOnAchievement);

        return $this->render('uspechy/index.html.twig', array(
            'yearsOnAchievement' => $yearsOnAchievement,
        ));
    }
    private function sortArrayByArray(array $arrayYears) {
        $sortedArray = array();
        foreach ($arrayYears as $year => $array){
            $orderArray = array("Speciální","1. místo","2. místo","3. místo","ostatní");
            foreach ($orderArray as $value){
                if (!array_key_exists($value, $array)){
                    $array[$value] = array();
                }
            }
            $ordered = array();
            foreach ($orderArray as $key) {
                if (array_key_exists($key, $array)) {
                    $ordered[$key] = $array[$key];
                    unset($array[$key]);
                }
            }
            $sortedArray[$year] = $ordered + $array;
        }

        return $sortedArray;
    }
}
