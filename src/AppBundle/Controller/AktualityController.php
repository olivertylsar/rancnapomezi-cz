<?php

namespace AppBundle\Controller;

use AppBundle\Entity\News;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AktualityController extends Controller
{
    /**
     * @Route("/aktuality", name="aktuality")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine();
        $news = $em->getRepository(News::class)->findBy(array(), array('date' => 'DESC'));
        return $this->render('aktuality/index.html.twig', array(
            'news' => $news
        ));
    }

    /**
     * @Route("/aktualita/{slug}", name="aktuality_detail")
     * @param News $news
     * @return Response
     */
    public function detailAction(Request $request, News $news)
    {
        return $this->render('aktuality/detail.html.twig', array(
            'news' => $news
        ));
    }
}
