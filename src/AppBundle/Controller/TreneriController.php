<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class TreneriController extends Controller
{
    /**
     * @Route("/treneri", name="treneri")
     */
    public function indexAction(Request $request)
    {
        return $this->render('treneri/index.html.twig');
    }
}
