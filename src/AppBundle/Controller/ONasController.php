<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ONasController extends Controller
{
    /**
     * @Route("/o-nas", name="o-nas")
     */
    public function indexAction(Request $request)
    {
        return $this->render('o-nas/index.html.twig');
    }
}