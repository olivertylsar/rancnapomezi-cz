<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class HalaController extends Controller
{
    /**
     * @Route("/hala", name="hala")
     */
    public function indexAction(Request $request)
    {
        return $this->render('hala/index.html.twig');
    }
}