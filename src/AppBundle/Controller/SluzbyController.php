<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SluzbyController extends Controller
{
    /**
     * @Route("/sluzby", name="sluzby")
     */
    public function indexAction(Request $request)
    {
        return $this->render('sluzby/index.html.twig');
    }
}