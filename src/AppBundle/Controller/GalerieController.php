<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Gallery;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class GalerieController extends Controller
{
    /**
     * @Route("/galerie", name="galerie")
     */
    public function indexAction(Request $request)
    {
    	$galleries = $this->getDoctrine()->getRepository(Gallery::class)->findBy(array(), array(
    	    'id' => 'DESC'
        ));
        return $this->render('galerie/index.html.twig', array(
        	'galleries' => $galleries
        ));
    }
}