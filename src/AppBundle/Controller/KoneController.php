<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Horse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class KoneController extends Controller
{
    /**
     * @Route("/kone", name="kone")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $plemenniHrebciCat = $em->getRepository('AppBundle:HorseCategory')->find(1);
        $chovneKlisnyCat = $em->getRepository('AppBundle:HorseCategory')->find(2);
        $ostatniKone = $em->getRepository('AppBundle:Horse')->findBy(array(
            'horseCategory' => 3,
            'sports' => false,
            'horseSale' => false
        ));
        $naProdej = $em->getRepository('AppBundle:Horse')->findBy(array('horseSale' => true));
        $sportovniKone = $em->getRepository('AppBundle:Horse')->findBy(array('sports' => true));

        $categories = array(
            'plemenniHrebci' => array('slug' => 'plemenni-hrebci', 'title' => $plemenniHrebciCat->getName(), 'horses' => $plemenniHrebciCat->getHorses()),
            'chovneKlisny' => array('slug' => 'chovne-kobyly', 'title' => $chovneKlisnyCat->getName(), 'horses' => $chovneKlisnyCat->getHorses()),
            'naProdej' => array('slug' => 'na-prodej', 'title' => 'Na prodej', 'horses' => $naProdej),
            'sportovniKone' => array('slug' => 'sportovni-kone', 'title' => 'Sportovní koně', 'horses' => $sportovniKone),
            'ostatniKone' => array('slug' => 'ostatni-kone', 'title' => 'Ostatní koně', 'horses' => $ostatniKone)
        );

        return $this->render('kone/index.html.twig', array(
            'categories' => $categories
        ));
    }

    /**
     * @Route("/kun/{slug}", name="kone_detail")
     * @param Request $request
     * @param Horse $horse
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailAction(Request $request, Horse $horse)
    {
        $em = $this->getDoctrine()->getManager();

        return $this->render('kone/detail.html.twig', array(
            'horse' => $horse
        ));
    }
}
