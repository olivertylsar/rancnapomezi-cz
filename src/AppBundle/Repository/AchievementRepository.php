<?php
/**
 * Created by PhpStorm.
 * User: Jirka
 * Date: 1/4/2018
 * Time: 12:20 AM
 */

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class AchievementRepository extends EntityRepository
{
    public function getAchievementYears()
    {
        $qb = $this->createQueryBuilder('a')
            ->addSelect('YEAR(a.date) AS year')
            ->leftJoin('a.achievementCategory', 'ac')
            ->groupBy('year')
            ->orderBy('a.date', 'DESC');
        return $qb->getQuery()->getResult();
    }

    public function findByYear($year)
    {
        $qb = $this->createQueryBuilder('a')
            ->andWhere('YEAR(a.date) =:year')
            ->setParameter('year', $year);
        return $qb->getQuery()->getResult();
    }

}