<?php
/**
 * Created by PhpStorm.
 * User: Jirka
 * Date: 1/16/2018
 * Time: 10:03 AM
 */

namespace AdminBundle\Service;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\HttpFoundation\Request;

class FileUploaderService
{
    private $fileSystem;
    private $container;

    const million = 1000000;

    public function __construct(ContainerInterface $container)
    {
        $this->fileSystem = new Filesystem();
        $this->container = $container;
    }

    public function upload(UploadedFile $file, string $targetDir)
    {
        $fileName = md5(uniqid()).'.'.$file->guessExtension();
        $file->move($targetDir, $fileName);

        return $fileName;
    }

    public function remove(string $file, string $targetDir)
    {
        $oldFile = $targetDir.'/'.$file;
        $this->fileSystem->remove($oldFile);
    }

    public function checkMaxFilesSize($photos, Request $request)
    {
            if ($this->container->getParameter('max_post_content_length') < (int)$request->server->get('CONTENT_LENGTH')){
                return array('isOk' => false,
                    'message' => 'Celková velikost nahrávaných souborů je příliš velká, maximální povolená velikost je '.($this->container->getParameter('max_post_content_length')/self::million).'MB.');
            }
            foreach ($photos as $photo){
                $file = $photo->getFile();
                if($file instanceof UploadedFile){
                    if ($file->getClientMimeType() != 'image/jpeg' && $file->getClientMimeType() != 'image/png'){
                        return array('isOk' => false,
                            'message' => 'Nesprávný formát souboru '.$file->getClientOriginalName().'! Povolené formáty: png, jpg');
                    }elseif ($file->getClientSize() > $this->container->getParameter('max_upload_size')){
                        return array('isOk' => false,
                            'message' => 'Soubor '.$file->getClientOriginalName().' je příliš velký! Maximání povolená velikost je '.($this->container->getParameter('max_upload_size')/self::million).'MB.');
                    }
                }
            }
            return array('isOk' => true);
    }

    public function checkMaxFileSize($photo, Request $request)
    {
        if ($this->container->getParameter('max_post_content_length') < (int)$request->server->get('CONTENT_LENGTH')){
            return array('isOk' => false,
                'message' => 'Celková velikost nahrávaných souborů je příliš velká, maximální povolená velikost je '.($this->container->getParameter('max_post_content_length')/self::million).'MB.');
        }
        else{
            if($photo instanceof UploadedFile){
                if ($photo->getClientMimeType() != 'image/jpeg' && $photo->getClientMimeType() != 'image/png'){
                    return array('isOk' => false,
                        'message' => 'Nesprávný formát souboru '.$photo->getClientOriginalName().'! Povolené formáty: png, jpg');
                }elseif ($photo->getClientSize() > $this->container->getParameter('max_upload_size')){
                    return array('isOk' => false,
                        'message' => 'Soubor '.$photo->getClientOriginalName().' je příliš velký! Maximání povolená velikost je '.($this->container->getParameter('max_upload_size')/self::million).'MB.');
                }
            }
        }

        return array('isOk' => true);
    }
}