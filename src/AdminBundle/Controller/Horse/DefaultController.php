<?php
/**
 * Created by PhpStorm.
 * User: henzigo
 * Date: 9/11/17
 * Time: 1:08 PM
 */

namespace AdminBundle\Controller\Horse;

use AdminBundle\Form\HorseType;
use AppBundle\Entity\Horse;
use AppBundle\Entity\HorsePhoto;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Filesystem\Filesystem;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class DefaultController
 * @Route("/horse")
 */
class DefaultController extends Controller {

    /**
     * @return Response
     * @Route("/", name="admin_horse_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $horses = $em->getRepository(Horse::class)->findAll();

        return $this->render('@Admin/horse/index.html.twig', [
            'horses' => $horses
        ]);
    }

    /**
     * @param Request $request
     * @param Horse $horse
     * @return Response
     * @Route("/{id}/edit", name="admin_horse_edit", options={"expose" = true})
     * @Route("/new", name="admin_horse_new")
     */
    public function editAction(Horse $horse = null, Request $request) {

        if($horse === null) {
            $horse = new Horse();
        }

        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(HorseType::class, $horse);
        $form->handleRequest($request);
        $session = $request->getSession();
        if (!$form->isSubmitted()){
            $session->remove('oldHorsePhotos');
            $session->remove('familyTree');
            $session->remove('mainPhoto');
        }
        if (!$session->has('oldHorsePhotos')){
            $oldPhotos = array();
            foreach ($horse->getHorsePhotos() as $photo){
                $oldPhotos[] = $photo;
            }
            $session->set('oldHorsePhotos', $oldPhotos);
        }
        if (!$session->has('familyTree')){
            $session->set('familyTree', array($horse->getFamilyTree(), $horse->getFamilyTreeFileName()));
        }
	    if (!$session->has('mainPhoto')){
		    $session->set('mainPhoto', array($horse->getMainPhoto(), $horse->getMainPhotoFileName()));
	    }
        if ($form->isSubmitted()) {
            try {
                $uploaderService = $this->get('admin.file_uploader');
                $dirPath = $this->getParameter('horse_directory');
                $oldPhotos = $session->get('oldHorsePhotos');
                $oldFamilyTree = $session->get('familyTree')[0];
	            $familyTree = $horse->getFamilyTree();
                $oldMainPhoto = $session->get('mainPhoto')[0];
	            $mainPhoto = $horse->getMainPhoto();
                $uploadCheck = $uploaderService->checkMaxFilesSize($horse->getHorsePhotos(), $request);
                $uploadCheckFile = $uploaderService->checkMaxFileSize($familyTree, $request);
                $uploadCheckFileMainPhoto = $uploaderService->checkMaxFileSize($mainPhoto, $request);

                if (!$uploadCheck['isOk']){
                    throw new \Exception($uploadCheck['message']);
                }
                if (!$uploadCheckFile['isOk']){
                    throw new \Exception($uploadCheckFile['message']);
                }
	            if (!$uploadCheckFileMainPhoto['isOk']){
		            throw new \Exception($uploadCheckFile['message']);
	            }
                foreach ($horse->getHorsePhotos() as $key => $photo){
                    $file = $photo->getFile();
                    if($file instanceof UploadedFile){
                        $fileName = $uploaderService->upload($file, $dirPath);
                        $photo->setFile($fileName);
                        $photo->setFileName($file->getClientOriginalName());
                    }
                    else{
                        $photo->setFile($oldPhotos[$key]->getFile());
                    }
                }

                if ($familyTree instanceof UploadedFile){
                    if ($oldFamilyTree){
                        $uploaderService->remove($oldFamilyTree, $dirPath);
                    }
                    $fileName = $uploaderService->upload($familyTree, $dirPath);
                    $horse->setFamilyTree($fileName);
                    $horse->setFamilyTreeFileName($familyTree->getClientOriginalName());
                }
                else{
                    $horse->setFamilyTree($oldFamilyTree);
                }
//				dump($mainPhoto);die;
	            if ($mainPhoto instanceof UploadedFile){
		            if ($oldMainPhoto){
			            $uploaderService->remove($oldMainPhoto, $dirPath);
		            }
		            $fileNamePhoto = $uploaderService->upload($mainPhoto, $dirPath);
		            $horse->setMainPhoto($fileNamePhoto);
		            $horse->setMainPhotoFileName($mainPhoto->getClientOriginalName());
	            }
	            else{
		            $horse->setMainPhoto($oldMainPhoto);
	            }
                $session->remove('oldHorsePhotos');
                $session->remove('familyTree');
	            $session->remove('mainPhoto');
                $em->persist($horse);
                $em->flush();
                $em->clear();

                return $this->redirectToRoute('admin_horse_index');
            } catch (\Exception $e) {
                $session->remove('oldHorsePhotos');
                $session->remove('familyTree');
	            $session->remove('mainPhoto');
                $this->addFlash(
                    'error',
                    $e->getMessage()
                );
                return $this->redirect($request->getUri());
            }
        }
        return $this->render('@Admin/horse/edit.html.twig', [
            'form' => $form->createView(),
            'oldHorsePhotos' => $session->get('oldHorsePhotos'),
            'oldFamilyTree' => $session->get('familyTree')[1],
            'oldMainPhoto' => $session->get('mainPhoto')[1],
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/photo/delete", name="admin_horse_photo_delete")
     */
    public function deletePhotoAction(Request $request)
    {
        $uploaderService = $this->get('admin.file_uploader');
        $dirPath = $this->getParameter('horse_directory');
        $data =  $request->get('data');
        $photoId = $data['photoId'];
        $em = $this->getDoctrine()->getManager();
        $horsePhoto = $em->getRepository(HorsePhoto::class)->findOneBy(array('id' => $photoId));
        $uploaderService->remove($horsePhoto->getFile(), $dirPath);
        $em->remove($horsePhoto);
        $em->flush();
        $em->clear();
        return new JsonResponse(array('success' => true, 'code' => 200));
    }

    /**
     * @param Horse $horse
     * @return Response
     * @Route("/delete/{id}", name="admin_horse_delete")
     */
    public function deleteAction(Horse $horse)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($horse);
        $em->flush();
        return $this->redirectToRoute('admin_horse_index');
    }

}