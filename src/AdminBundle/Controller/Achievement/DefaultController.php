<?php
/**
 * Created by PhpStorm.
 * User: henzigo
 * Date: 9/11/17
 * Time: 1:08 PM
 */

namespace AdminBundle\Controller\Achievement;

use AdminBundle\Form\AchievementType;
use AdminBundle\Form\NewsType;
use AppBundle\Entity\Achievement;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Filesystem\Filesystem;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class DefaultController
 * @Route("/achievement")
 */
class DefaultController extends Controller {

    /**
     * @return Response
     * @Route("/", name="admin_achievement_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $achievements = $em->getRepository(Achievement::class)->findAll();

        return $this->render('@Admin/achievement/index.html.twig', [
            'achievements' => $achievements
        ]);
    }

    /**
     * @param Request $request
     * @param Achievement $achievement
     * @return Response
     * @Route("/{id}/edit", name="admin_achievement_edit", options={"expose" = true})
     * @Route("/new", name="admin_achievement_new")
     */
    public function editAction(Achievement $achievement = null, Request $request) {

        if($achievement === null) {
            $achievement = new Achievement();
        }

        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(AchievementType::class, $achievement);
        $form->handleRequest($request);
        
        if ($form->isSubmitted()) {
            try {
                $em->persist($achievement);
                $em->flush();
                $em->clear();

                return $this->redirectToRoute('admin_achievement_index');
            } catch (\Exception $e) {
            }
        }
        return $this->render('@Admin/achievement/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param Achievement $achievement
     * @return Response
     * @Route("/delete/{id}", name="admin_achievement_delete")
     */
    public function deleteAction(Achievement $achievement)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($achievement);
        $em->flush();
        return $this->redirectToRoute('admin_achievement_index');
    }

}