<?php
/**
 * Created by PhpStorm.
 * User: henzigo
 * Date: 9/11/17
 * Time: 1:08 PM
 */

namespace AdminBundle\Controller\News;

use AdminBundle\Form\NewsType;
use AppBundle\Entity\News;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Filesystem\Filesystem;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class DefaultController
 * @Route("/news")
 */
class DefaultController extends Controller {

	const MAX_LENGTH_IMAGE_NAME = 100;

    /**
     * @return Response
     * @Route("/", name="admin_news_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $news = $em->getRepository(News::class)->findBy(array(), array('date' => 'DESC'));

        return $this->render('@Admin/news/index.html.twig', [
            'news' => $news
        ]);
    }

    /**
     * @param Request $request
     * @param News $news
     * @return Response
     * @Route("/{id}/edit", name="admin_news_edit", options={"expose" = true})
     * @Route("/new", name="admin_news_new")
     */
    public function editAction(News $news = null, Request $request) {

        if($news === null) {
            $news = new News();
        }

        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(NewsType::class, $news);

        $form->handleRequest($request);
        $session = $request->getSession();
        if (!$form->isSubmitted()){
            $session->remove('oldImage');
        }
        if (!$session->has('oldImage')){
            $session->set('oldImage', array($news->getImage(), $news->getImageName()));
        }
        if ($form->isSubmitted()) {
            try {
                $uploaderService = $this->get('admin.file_uploader');
                $dirPath = $this->getParameter('news_directory');
                $oldImage = $session->get('oldImage')[0];
                $image = $news->getImage();
                $uploadCheckFile = $uploaderService->checkMaxFileSize($image, $request);
                if (!$uploadCheckFile['isOk']){
                    throw new \Exception($uploadCheckFile['message']);
                }

                if ($image instanceof UploadedFile){
                    if($oldImage){
                        $uploaderService->remove($oldImage, $dirPath);
                    }
                    $fileName = $uploaderService->upload($image, $dirPath);
                    $news->setImage($fileName);
	                if (strlen($image->getClientOriginalName()) < self::MAX_LENGTH_IMAGE_NAME) {
		                $news->setImageName($image->getClientOriginalName());
	                } else {
		                throw new \Exception('Název nahravaného obrázku obsahuje ' . strlen($image->getClientOriginalName()) . ' znaků, nesmí však přesáhnout:' . self::MAX_LENGTH_IMAGE_NAME . 'znaků!');
	                }
                }
                else{
                    $news->setImage($oldImage);
                }

                $session->remove('oldImage');
                $em->persist($news);
                $em->flush();
                $em->clear();

                return $this->redirectToRoute('admin_news_index');
            } catch (\Exception $e) {
                $session->remove('oldImage');
                $this->addFlash(
                    'error',
                    $e->getMessage()
                );
                return $this->redirect($request->getUri());
            }
        }
        return $this->render('@Admin/news/edit.html.twig', [
            'form' => $form->createView(),
            'oldImage' => $session->get('oldImage')[1]
        ]);
    }

    /**
     * @param News $news
     * @return Response
     * @Route("/delete/{id}", name="admin_news_delete")
     */
    public function deleteAction(News $news)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($news);
        $em->flush();
        return $this->redirectToRoute('admin_news_index');
    }

}