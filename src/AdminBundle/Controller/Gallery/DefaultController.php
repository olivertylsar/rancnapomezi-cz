<?php
/**
 * Created by PhpStorm.
 * User: henzigo
 * Date: 9/11/17
 * Time: 1:08 PM
 */

namespace AdminBundle\Controller\Gallery;

use AdminBundle\Form\GalleryType;
use AdminBundle\Form\HorseType;
use AppBundle\Entity\Gallery;
use AppBundle\Entity\Horse;
use AppBundle\Entity\Photo;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Filesystem\Filesystem;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class DefaultController
 * @Route("/gallery")
 */
class DefaultController extends Controller {

    /**
     * @return Response
     * @Route("/", name="admin_gallery_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $galleries = $em->getRepository(Gallery::class)->findAll();

        return $this->render('@Admin/gallery/index.html.twig', [
            'galleries' => $galleries
        ]);
    }

    /**
     * @param Request $request
     * @param Gallery $gallery
     * @return Response
     * @Route("/{id}/edit", name="admin_gallery_edit", options={"expose" = true})
     * @Route("/new", name="admin_gallery_new")
     */
    public function editAction(Gallery $gallery = null, Request $request) {

        if($gallery === null) {
            $gallery = new Gallery();
        }

        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(GalleryType::class, $gallery);
        $form->handleRequest($request);
        $session = $request->getSession();
        if (!$form->isSubmitted()){
            $session->remove('oldPhotos');
        }
        if (!$session->has('oldPhotos')){
            $oldPhotos = array();
            foreach ($gallery->getPhotos() as $photo){
                $oldPhotos[] = $photo;
            }
            $session->set('oldPhotos', $oldPhotos);
        }
        if ($form->isSubmitted()) {
            try {
                $uploaderService = $this->get('admin.file_uploader');
                $dirPath = $this->getParameter('gallery_directory');
                $oldPhotos = $session->get('oldPhotos');
                $uploadCheck = $uploaderService->checkMaxFilesSize($gallery->getPhotos(), $request);
                if (!$uploadCheck['isOk']){
                    throw new \Exception($uploadCheck['message']);
                }

                foreach ($gallery->getPhotos() as $key => $photo){
                    $file = $photo->getFile();
                    if($file instanceof UploadedFile){
                        if (key_exists($key,$oldPhotos)){
                            $uploaderService->remove($oldPhotos[$key]->getFile(), $dirPath);
                        }
                        $fileName = $uploaderService->upload($file, $dirPath);
                        $photo->setFile($fileName);
                        $photo->setFileName($file->getClientOriginalName());
                    }
                    else{
                        $photo->setFile($oldPhotos[$key]->getFile());
                    }
                }
                $session->remove('oldPhotos');
                $em->persist($gallery);
                $em->flush();
                $em->clear();

                return $this->redirectToRoute('admin_gallery_index');
            } catch (\Exception $e) {
                $session->remove('oldPhotos');
                $this->addFlash(
                    'error',
                    $e->getMessage()
                );
                return $this->redirect($request->getUri());
            }
        }
        return $this->render('@Admin/gallery/edit.html.twig', [
            'form' => $form->createView(),
            'oldPhotos' => $session->get('oldPhotos')
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/photo/delete", name="admin_gallery_photo_delete")
     */
    public function deletePhotoAction(Request $request)
    {
        $uploaderService = $this->get('admin.file_uploader');
        $dirPath = $this->getParameter('gallery_directory');
        $data =  $request->get('data');
        $photoId = $data['photoId'];
        $em = $this->getDoctrine()->getManager();
        $photo = $em->getRepository(Photo::class)->findOneBy(array('id' => $photoId));
        $uploaderService->remove($photo->getFile(), $dirPath);
        $em->remove($photo);
        $em->flush();
        $em->clear();
        return new JsonResponse(array('success' => true, 'code' => 200));
    }

    /**
     * @param Gallery $gallery
     * @return Response
     * @Route("/delete/{id}", name="admin_gallery_delete")
     */
    public function deleteAction(Gallery $gallery)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($gallery);
        $em->flush();
        return $this->redirectToRoute('admin_gallery_index');
    }

}