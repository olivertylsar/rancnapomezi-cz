<?php
/**
 * Created by PhpStorm.
 * User: Jirka
 * Date: 1/12/2018
 * Time: 11:12 AM
 */

namespace AdminBundle\Form;

use AppBundle\Entity\Category;
use AppBundle\Entity\Harnessing;
use AppBundle\Entity\Horse;
use AppBundle\Entity\HorseCategory;
use AppBundle\Entity\News;
use AppBundle\Entity\SexCategory;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class HorseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('horseCategory', EntityType::class,array(
                'class' => HorseCategory::class,
                'choice_label' => 'name',
                'label' => 'Kategorie',
            ))
            ->add('name', TextType::class,array(
                'label' => 'Jméno'
            ))
            ->add('birthDate', BirthdayType::class, array(
                'label' => 'Datum narození'
            ))
            ->add('father', TextType::class, array(
                'label' => 'Otec',
            ))
            ->add('mother', TextType::class, array(
                'label' => 'Matka',
            ))
            ->add('birthPlace', TextType::class, array(
                'label' => 'Místo narození',
                'required' => false
            ))
            ->add('sexCategory', EntityType::class,array(
                'class' => SexCategory::class,
                'choice_label' => 'name',
                'label' => 'Pohlaví',
            ))
            ->add('color', TextType::class, array(
                'label' => 'Barva',
                'required' => false
            ))
            ->add('children', TextType::class, array(
		        'label' => 'Potomci',
		        'required' => false
	        ))
	        ->add('comment', TextType::class, array(
		        'label' => 'Poznámka',
		        'required' => false
	        ))
	        ->add('harnessingTax', TextType::class, array(
		        'label' => 'Připouštěcí poplatek',
		        'required' => false
	        ))
	        ->add('owner', TextType::class, array(
		        'label' => 'Majitel',
		        'required' => false
	        ))
	        ->add('harnessings', EntityType::class, array(
	        	'class' => Harnessing::class,
		        'label' => 'Možnosti připouštění',
		        'query_builder' => function (EntityRepository $er) {
			        return $er->createQueryBuilder('h')
				        ->orderBy('h.name', 'ASC');
		        },
		        'choice_label' => 'name',
		        'required' => false,
		        'multiple' => true,
		        'expanded' => true
	        ))
            ->add('familyTree', FileType::class, [
                'required' => false,
                'label' => 'Rodokmen',
                'data_class' => null,
            ])
            ->add('horsePhotos', CollectionType::class, array(
                'label' => 'Fotky koně',
                'entry_type' => AddHorsePhotoType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype_name' => '___photo___',
                'by_reference' => false
            ))
	        ->add('sports', CheckboxType::class, array(
		        'label' => 'Sportovní',
		        'required' => false
	        ))
	        ->add('horseSale', CheckboxType::class, array(
		        'label' => 'Na prodej',
		        'required' => false
	        ))
	        ->add('mainPhoto', FileType::class, [
		        'required' => false,
		        'label' => 'Hlavní fotografie',
		        'data_class' => null,
	        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Horse::class,
        ]);

    }

}