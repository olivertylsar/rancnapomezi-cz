<?php
/**
 * Created by PhpStorm.
 * User: Jirka
 * Date: 1/12/2018
 * Time: 11:12 AM
 */

namespace AdminBundle\Form;

use AppBundle\Entity\Gallery;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GalleryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class,array(
                'label' => 'Název'
            ))
            ->add('rajceLink', TextType::class, array(
                'label' => 'Link na rajče',
            ))
            ->add('photos', CollectionType::class, array(
                'label' => 'Fotky galerie',
                'entry_type' => AddGalleryPhotoType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype_name' => '___photo___',
                'by_reference' => false,
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Gallery::class,
        ]);

    }
}