<?php
/**
 * Created by PhpStorm.
 * User: Jirka
 * Date: 1/12/2018
 * Time: 11:12 AM
 */

namespace AdminBundle\Form;

use AppBundle\Entity\HorsePhoto;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddHorsePhotoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('file', FileType::class, [
                'required' => false,
                'label' => 'Obrázek',
                'data_class' => null,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => HorsePhoto::class,
        ]);

    }

}