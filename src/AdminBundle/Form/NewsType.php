<?php
/**
 * Created by PhpStorm.
 * User: Jirka
 * Date: 1/12/2018
 * Time: 11:12 AM
 */

namespace AdminBundle\Form;

use AppBundle\Entity\Category;
use AppBundle\Entity\News;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class NewsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('category', EntityType::class,array(
                'class' => Category::class,
                'choice_label' => 'name',
                'label' => 'Kategorie',
            ))
            ->add('title', TextType::class,array(
                'label' => 'Název'
            ))
            ->add('place', TextType::class,array(
                'label' => 'Místo'
            ))
            ->add('date', DateTimeType::class, array(
                'label' => 'Datum'
            ))
            ->add('description', TextareaType::class, array(
                'label' => 'Popis'
            ))
            ->add('image', FileType::class, [
                'required' => false,
                'label' => 'Obrázek',
                'data_class' => null,
            ])
            ->add('fbLink', TextType::class, array(
                'label' => 'Link na FB',
                'required' => false,
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => News::class,
        ]);

    }

}