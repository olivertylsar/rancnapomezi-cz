<?php
/**
 * Created by PhpStorm.
 * User: Jirka
 * Date: 1/12/2018
 * Time: 11:12 AM
 */

namespace AdminBundle\Form;

use AppBundle\Entity\Achievement;
use AppBundle\Entity\AchievementCategory;
use AppBundle\Entity\Horse;
use const Grpc\CHANNEL_IDLE;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class AchievementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date', BirthdayType::class,array(
                'label' => 'Datum'
            ))
            ->add('achievementCategory', EntityType::class,array(
                'class' => AchievementCategory::class,
                'choice_label' => 'name',
                'label' => 'Umístění',
            ))
            ->add('category', TextType::class,array(
                'label' => 'Kategorie',
            ))
            ->add('awardDriver', TextType::class,array(
                'label' => 'Jezdec',
                'required' => false
            ))
            ->add('horse', EntityType::class,array(
                'label' => 'Kůň',
                'class' => Horse::class,
                'choice_label' => 'name'
            ))
            ->add('race', TextType::class,array(
                'label' => 'Závod/Ocenění',
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Achievement::class,
        ]);

    }

}