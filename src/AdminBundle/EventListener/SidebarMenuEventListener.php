<?php

namespace AdminBundle\EventListener;

use SbS\AdminLTEBundle\Event\SidebarMenuEvent;
use SbS\AdminLTEBundle\Model\MenuItemModel;
use SbS\AdminLTEBundle\Model\MenuItemInterface;


class SidebarMenuEventListener
{
    private $menu;

    public function __construct()
    {

    }

    /**
     * @param SidebarMenuEvent $event
     */
    public function onShowMenu(SidebarMenuEvent $event)
    {
        foreach ($this->getMenu() as $index=>$item) {
            $item->setId($index);
            $event->addItem($item);
        }
    }

    /**
     * @return array
     */
    protected function getMenu()
    {
        $this->menu = array();
        $this->getNewsMenuItems();
        $this->getHorsesMenuItems();
        $this->getAchievementsMenuItems();
        $this->getGalleriesMenuItems();

        return $this->menu;
    }


    private function getNewsMenuItems(){
        $news = (new MenuItemModel('Aktuality'))->setRoute('admin_index')
            ->setIcon('fa fa-newspaper-o');
        $news->addChild((new MenuItemModel('Seznam'))->setRoute('admin_news_index'));
        $news->addChild((new MenuItemModel('Přidat aktualitu'))->setRoute('admin_news_new'));
        array_push($this->menu, $news);
    }

    private function getHorsesMenuItems(){

        $horses = (new MenuItemModel('Koně'))->setRoute('admin_horse_index')
            ->setIcon('fa fa-sitemap');
        $horses->addChild((new MenuItemModel('Seznam'))->setRoute('admin_horse_index'));
        $horses->addChild((new MenuItemModel('Přidat koně'))->setRoute('admin_horse_new'));
        array_push($this->menu, $horses);
    }

    private function getAchievementsMenuItems(){

        $achievements = (new MenuItemModel('Úspěchy'))->setRoute('admin_achievement_index')
            ->setIcon('fa fa-trophy');
        $achievements->addChild((new MenuItemModel('Seznam'))->setRoute('admin_achievement_index'));
        $achievements->addChild((new MenuItemModel('Přidat úspěch'))->setRoute('admin_achievement_new'));
        array_push($this->menu, $achievements);
    }

    private function getGalleriesMenuItems(){

        $galleries = (new MenuItemModel('Galerie'))->setRoute('admin_gallery_index')
            ->setIcon('fa fa-photo');
        $galleries->addChild((new MenuItemModel('Seznam'))->setRoute('admin_gallery_index'));
        $galleries->addChild((new MenuItemModel('Přidat galerii'))->setRoute('admin_gallery_new'));
        array_push($this->menu, $galleries);
    }

}