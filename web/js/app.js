(function($) {
  "use strict"; // Start of use strict

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top - 57)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#mainNav',
    offset: 57
  });

  // Collapse Navbar
  var navbarCollapse = function() {
    var route = $('#mainNav').data('route');
    if (route === 'homepage'){
      if ($("#mainNav").offset().top > 50) {
        $("#mainNav").addClass("navbar-shrink");
      } else {
        $("#mainNav").removeClass("navbar-shrink");
      }
    }
  };
  // Collapse now if page is not at top
  navbarCollapse();
  // Collapse the navbar when page is scrolled
  $(window).scroll(navbarCollapse);

  // Scroll reveal calls
  window.sr = ScrollReveal();
  sr.reveal('.first-slide h1', {
      duration: 2000,
      delay: 500,
      distance: '100px'
  });
  sr.reveal('.first-slide h2', {
      duration: 2000,
      delay: 500,
      distance: '100px'
  });
  sr.reveal('.first-slide .divider-inner', {
      duration: 2500,
      delay: 0,
      distance: '300px'
  });
  sr.reveal('.btn-animate-wrapper', {
      duration: 2000,
      delay: 1000,
      distance: '500px'
  });

})(jQuery); // End of use strict
