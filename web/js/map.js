$(function () {

    function initMap() {

        var location = new google.maps.LatLng(49.920080, 17.910259);

        var mapCanvas = document.getElementById('map');
        var mapOptions = {
            center: new google.maps.LatLng(49.920900, 17.910259),
            zoom: 16,
            panControl: false,
            scrollwheel: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(mapCanvas, mapOptions);

        var markerImage = {
            path: "M27.648-41.399q0-3.816-2.7-6.516t-6.516-2.7-6.516 2.7-2.7 6.516 2.7 6.516 6.516 2.7 6.516-2.7 2.7-6.516zm9.216 0q0 3.924-1.188 6.444l-13.104 27.864q-.576 1.188-1.71 1.872t-2.43.684-2.43-.684-1.674-1.872l-13.14-27.864q-1.188-2.52-1.188-6.444 0-7.632 5.4-13.032t13.032-5.4 13.032 5.4 5.4 13.032z",
            fillColor: '#56ba7e',
            fillOpacity: 1,
            strokeWeight: 0,
            scale: 0.6
        };

        var marker = new google.maps.Marker({
            position: location,
            map: map,
            icon: markerImage
        });

        var contentString = '<div class="info-window">' +
            '<h3 class="mt-3">Ranč na pomezí</h3>' +
            '<div class="info-content">' +
            '<p class="mb-0">Na Pomezí 83</p>'+
            '<p>74706 Opava 6</p>' +
            '</div>' +
            '</div>';

        var infowindow = new google.maps.InfoWindow({
            content: contentString,
            maxWidth: 400
        });

        marker.addListener('click', function () {
            infowindow.open(map, marker);
        });
        infowindow.open(map,marker);
    }

    $(document).ready(function () {
        initMap();
    });

    google.maps.event.addDomListener(window, 'load', initMap);
});
