window.addEventListener("load", function(){
    window.cookieconsent.initialise({
        "palette": {
            "popup": {
                "background": "#f8f8f8",
                "text": "#5c504c"
            },
            "button": {
                "background": "transparent",
                "text": "#5c504c",
                "border": "#f5deb3"
            }
        },
        "content": {
            "message": "Tento web používá k poskytování služeb cookies. Jeho používáním s tímto vyjadřujete souhlas.",
            "dismiss": "Souhlasím",
            "link": "Více informací"
        }
    })});