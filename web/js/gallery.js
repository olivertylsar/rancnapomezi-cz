$(document).ready(function() {
    $('.gallery').each(function () {
        $(this).find('.popup-gallery').magnificPopup({
            delegate: 'a',
            type: 'image',
            tClose: 'Zavřít (Esc)', // Alt text on close button
            tLoading: 'Načítání obrázku #%curr%...',
            mainClass: 'mfp-img-mobile',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                tPrev: 'Předchozí (Šipka vlevo)', // Alt text on left arrow
                tNext: 'Další (Šipka vpravo)', // Alt text on right arrow
                tCounter: '%curr%/%total%', // Markup for "1 of 7" counter
                preload: [0, 1] // Will preload 0 - before current, and 1 after the current image

            },
            image: {
                tError: '<a href="%url%">Obrázek #%curr%</a> se nepodařilo načíst.',
                cursor: null,
                titleSrc: function (item) {
                    return item.el.attr('title');
                },
                verticalFit: true
            }
        });
    })
});