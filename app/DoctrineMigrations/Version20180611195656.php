<?php declare(strict_types = 1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180611195656 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE harnessing (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE harnessings (horse_id INT NOT NULL, harnessing_id INT NOT NULL, INDEX IDX_A91CF57276B275AD (horse_id), INDEX IDX_A91CF5725EB06C2F (harnessing_id), PRIMARY KEY(horse_id, harnessing_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE harnessings ADD CONSTRAINT FK_A91CF57276B275AD FOREIGN KEY (horse_id) REFERENCES horse (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE harnessings ADD CONSTRAINT FK_A91CF5725EB06C2F FOREIGN KEY (harnessing_id) REFERENCES harnessing (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE horse ADD comment LONGTEXT DEFAULT NULL');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE harnessings DROP FOREIGN KEY FK_A91CF5725EB06C2F');
        $this->addSql('DROP TABLE harnessing');
        $this->addSql('DROP TABLE harnessings');
        $this->addSql('ALTER TABLE horse DROP comment');
    }
}
