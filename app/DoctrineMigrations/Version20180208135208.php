<?php declare(strict_types = 1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180208135208 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE horse ADD family_tree_file_name LONGTEXT NOT NULL');
        $this->addSql('ALTER TABLE horse_photo ADD file_name VARCHAR(50) NOT NULL');
        $this->addSql('ALTER TABLE photo ADD file_name VARCHAR(50) NOT NULL');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE horse DROP family_tree_file_name');
        $this->addSql('ALTER TABLE horse_photo DROP file_name');
        $this->addSql('ALTER TABLE photo DROP file_name');
    }
}
