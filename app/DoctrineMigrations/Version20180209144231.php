<?php declare(strict_types = 1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180209144231 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE sex_category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE horse ADD sex_category_id INT DEFAULT NULL, DROP sex');
        $this->addSql('ALTER TABLE horse ADD CONSTRAINT FK_629A2F181F5514EC FOREIGN KEY (sex_category_id) REFERENCES sex_category (id)');
        $this->addSql('CREATE INDEX IDX_629A2F181F5514EC ON horse (sex_category_id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE horse DROP FOREIGN KEY FK_629A2F181F5514EC');
        $this->addSql('DROP TABLE sex_category');
        $this->addSql('DROP INDEX IDX_629A2F181F5514EC ON horse');
        $this->addSql('ALTER TABLE horse ADD sex TINYINT(1) NOT NULL, DROP sex_category_id');
    }
}
