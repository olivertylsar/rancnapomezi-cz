<?php declare(strict_types = 1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180207151359 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE achievement (id INT AUTO_INCREMENT NOT NULL, achievements INT DEFAULT NULL, date DATE NOT NULL, `order` INT NOT NULL, category VARCHAR(50) NOT NULL, award_driver VARCHAR(50) NOT NULL, race VARCHAR(50) NOT NULL, INDEX IDX_96737FF1D1227EFE (achievements), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE gallery (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(50) NOT NULL, rajce_link VARCHAR(100) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE horse (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(50) NOT NULL, name VARCHAR(50) NOT NULL, birth_date DATE NOT NULL, father VARCHAR(50) NOT NULL, mother VARCHAR(50) NOT NULL, birthPlace VARCHAR(50) NOT NULL, sex VARCHAR(50) NOT NULL, color VARCHAR(50) NOT NULL, children LONGTEXT NOT NULL, family_tree VARCHAR(50) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE horse_photo (id INT AUTO_INCREMENT NOT NULL, horse INT DEFAULT NULL, file VARCHAR(50) NOT NULL, INDEX IDX_AB354E29629A2F18 (horse), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE photo (id INT AUTO_INCREMENT NOT NULL, gallery INT DEFAULT NULL, file VARCHAR(50) NOT NULL, INDEX IDX_14B78418472B783A (gallery), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE achievement ADD CONSTRAINT FK_96737FF1D1227EFE FOREIGN KEY (achievements) REFERENCES horse (id)');
        $this->addSql('ALTER TABLE horse_photo ADD CONSTRAINT FK_AB354E29629A2F18 FOREIGN KEY (horse) REFERENCES horse (id)');
        $this->addSql('ALTER TABLE photo ADD CONSTRAINT FK_14B78418472B783A FOREIGN KEY (gallery) REFERENCES gallery (id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE photo DROP FOREIGN KEY FK_14B78418472B783A');
        $this->addSql('ALTER TABLE achievement DROP FOREIGN KEY FK_96737FF1D1227EFE');
        $this->addSql('ALTER TABLE horse_photo DROP FOREIGN KEY FK_AB354E29629A2F18');
        $this->addSql('DROP TABLE achievement');
        $this->addSql('DROP TABLE gallery');
        $this->addSql('DROP TABLE horse');
        $this->addSql('DROP TABLE horse_photo');
        $this->addSql('DROP TABLE photo');
    }
}
