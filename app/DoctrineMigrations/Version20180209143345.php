<?php declare(strict_types = 1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180209143345 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE achievement_category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE horse_category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE achievement ADD achievement_category_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE achievement ADD CONSTRAINT FK_96737FF15AF82E7D FOREIGN KEY (achievement_category_id) REFERENCES achievement_category (id)');
        $this->addSql('CREATE INDEX IDX_96737FF15AF82E7D ON achievement (achievement_category_id)');
        $this->addSql('ALTER TABLE horse ADD horse_category_id INT DEFAULT NULL, DROP category');
        $this->addSql('ALTER TABLE horse ADD CONSTRAINT FK_629A2F18F7977E0D FOREIGN KEY (horse_category_id) REFERENCES horse_category (id)');
        $this->addSql('CREATE INDEX IDX_629A2F18F7977E0D ON horse (horse_category_id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE achievement DROP FOREIGN KEY FK_96737FF15AF82E7D');
        $this->addSql('ALTER TABLE horse DROP FOREIGN KEY FK_629A2F18F7977E0D');
        $this->addSql('DROP TABLE achievement_category');
        $this->addSql('DROP TABLE horse_category');
        $this->addSql('DROP INDEX IDX_96737FF15AF82E7D ON achievement');
        $this->addSql('ALTER TABLE achievement DROP achievement_category_id');
        $this->addSql('DROP INDEX IDX_629A2F18F7977E0D ON horse');
        $this->addSql('ALTER TABLE horse ADD category INT NOT NULL, DROP horse_category_id');
    }
}
