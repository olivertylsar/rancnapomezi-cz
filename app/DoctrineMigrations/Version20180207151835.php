<?php declare(strict_types = 1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180207151835 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE achievement DROP FOREIGN KEY FK_96737FF1D1227EFE');
        $this->addSql('DROP INDEX IDX_96737FF1D1227EFE ON achievement');
        $this->addSql('ALTER TABLE achievement CHANGE achievements horse_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE achievement ADD CONSTRAINT FK_96737FF176B275AD FOREIGN KEY (horse_id) REFERENCES horse (id)');
        $this->addSql('CREATE INDEX IDX_96737FF176B275AD ON achievement (horse_id)');
        $this->addSql('ALTER TABLE news DROP FOREIGN KEY FK_1DD399501DD39950');
        $this->addSql('DROP INDEX IDX_1DD399501DD39950 ON news');
        $this->addSql('ALTER TABLE news CHANGE news category_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE news ADD CONSTRAINT FK_1DD3995012469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('CREATE INDEX IDX_1DD3995012469DE2 ON news (category_id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE achievement DROP FOREIGN KEY FK_96737FF176B275AD');
        $this->addSql('DROP INDEX IDX_96737FF176B275AD ON achievement');
        $this->addSql('ALTER TABLE achievement CHANGE horse_id achievements INT DEFAULT NULL');
        $this->addSql('ALTER TABLE achievement ADD CONSTRAINT FK_96737FF1D1227EFE FOREIGN KEY (achievements) REFERENCES horse (id)');
        $this->addSql('CREATE INDEX IDX_96737FF1D1227EFE ON achievement (achievements)');
        $this->addSql('ALTER TABLE news DROP FOREIGN KEY FK_1DD3995012469DE2');
        $this->addSql('DROP INDEX IDX_1DD3995012469DE2 ON news');
        $this->addSql('ALTER TABLE news CHANGE category_id news INT DEFAULT NULL');
        $this->addSql('ALTER TABLE news ADD CONSTRAINT FK_1DD399501DD39950 FOREIGN KEY (news) REFERENCES category (id)');
        $this->addSql('CREATE INDEX IDX_1DD399501DD39950 ON news (news)');
    }
}
